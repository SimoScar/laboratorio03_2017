package it.unimi.di.sweng.lab03;

public class IntegerNode {
	
	private Integer value = null;
	private IntegerNode next = null;
	private IntegerNode previous = null;
	
	public IntegerNode(int i) {
		value = i;
	}

	public IntegerNode next() {
		return next;
	}

	public void setNext(IntegerNode integerNode) {
		next = integerNode;		
	}

	public int getValue() {
		return value;
	}

	public IntegerNode previous() {
		return previous;
	}

	public void setPrevious(IntegerNode previous) {
		this.previous = previous;
	}
	

}
