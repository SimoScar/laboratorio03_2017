package it.unimi.di.sweng.lab03;
import java.io.InputStreamReader;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Scanner;
import java.util.function.IntPredicate;

public class IntegerList {
	
	private IntegerNode head= null;
	private IntegerNode tail=null;
	private IntegerNode lastVisited = null;
	
	public IntegerList(String stringValues){
		Scanner parser = new Scanner(stringValues);
		while (parser.hasNext()){
			String token = parser.next();
			addLast(Integer.parseInt(token));  // restuisce una sottoclasse di IllegalArgumentException 
		}										//se non passa							
		parser.close();
	}

	public IntegerList(InputStreamReader inputStreamReader) {
		Scanner parser = new Scanner(inputStreamReader);
		while (parser.hasNext()){
			String token = parser.next();
			addLast(Integer.parseInt(token));  // restuisce una sottoclasse di IllegalArgumentException 
		}										//se non passa							
		parser.close();
	}
	
	public IntegerList() {
		// TODO Auto-generated constructor stub
	}

	public String toString(){
		StringBuilder result= new StringBuilder("[");
		IntegerNode currentNode = head;
		int i =0;
		while(currentNode != null){
			if(i++ >0)
				result.append(" ");
			result.append(currentNode.getValue()); 
			currentNode = currentNode.next();
		}
		return result.append("]").toString();
	}
	
	private void firstNode(int value) {
		head = tail = new IntegerNode(value);
	}
	
	
	public void addLast(int value) {
		if(head == null){
			firstNode(value);
		}
		else{
			IntegerNode node = new IntegerNode(value);
			tail.setNext(node);
			node.setPrevious(tail);
			tail = node;
			}
	}

	

	public void addFirst(int value) {
		if(head == null){
			firstNode(value);
		}
		else{
			IntegerNode node = new IntegerNode(value);
			node.setNext(head);
			head.setPrevious(node);
			head = node;
		}
	}

	public boolean removeFirst() {
		if (head!= null){
			head = head.next();
			if (head == null)
				tail=null;
			return true;
		}
		
		return false;
	}

	public boolean removeLast() {
		if (tail!= null){
			tail = tail.previous();
			if (tail!= null)
				tail.setNext(null);
			else
				head=null;
			return true;
		}
		
		return false;
		
	}

	public boolean remove(int value) {
		IntegerNode currentNode = head;
		while (currentNode != null){
			if (currentNode.getValue()==value){
				currentNode.previous().setNext(currentNode.next());
				currentNode.next().setPrevious(currentNode.previous());
				return true;
			}
		currentNode = currentNode.next();
		}
		return false;
	}

	public boolean removeAll(int value) {
		IntegerNode currentNode = head;
		boolean changed = false;
		while (currentNode != null){
			if (currentNode.getValue()==value){
				currentNode.previous().setNext(currentNode.next());
				currentNode.next().setPrevious(currentNode.previous());
				changed = true;
				}
			currentNode = currentNode.next();
			}
		if (changed == true)
			return true;
		else
			return false;
	} //metodo molto simile a remove , però utilizza un boolean per verificare se il 
	 // metodo si è attivato 

	public double mean() {
		IntegerNode currentNode= head;
		double sum=0.0;
		int cont=0;
			while(currentNode != null){
				sum += (double)currentNode.getValue();
				cont++;
				currentNode = currentNode.next();
			}
		return sum/(double)cont;
	}

	public double stdDev() throws ParseException {
		
		IntegerNode currentNode = head;
		double stddev= 0.0;
		//double mean = this.mean();
		int cont=0;
		double totX=0.0;
			if( head == null || ( (head!=null) && head.next()==null)) // maybe every single number
				return 0.0;
			else{
				while(currentNode != null){
					totX += Math.pow(((double)currentNode.getValue() - this.mean()), 2);
					cont++;
					currentNode = currentNode.next();
				}
				totX = totX/(cont-1);
				stddev= Math.sqrt(totX); 				//la parte successiva di codice è legata solo
			    DecimalFormat df = new DecimalFormat("0.000");//alla troncatura dei decimali
			    df.setRoundingMode(RoundingMode.FLOOR);
				String formatted= df.format(stddev);
				double result = (Double)df.parse(formatted);
				return result;
			}
			
	}

	public int prev() {
		if(lastVisited == null)
			lastVisited=head;
			if (head == null)
				throw new NullPointerException("lista vuota");
			
		if(lastVisited != null){
			int nodo= lastVisited.getValue();
			if (lastVisited.previous() != null)
				lastVisited= lastVisited.previous();
			
			return nodo;
		}
		else 
			return 0;
	}

	public int  next() {
		if(lastVisited == null)
			lastVisited=head;
			if (head == null)
				throw new NullPointerException("lista vuota");
			
		if(lastVisited != null){
			int nodo= lastVisited.getValue();
			if (lastVisited.next() != null)
				lastVisited= lastVisited.next();
			
			return nodo;
		}
		else 
			return 0;
		
	}

}
